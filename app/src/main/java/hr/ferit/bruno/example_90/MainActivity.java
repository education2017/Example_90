package hr.ferit.bruno.example_90;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {
    private static String TAG = "MyDBApp";

    @BindView(R.id.bAddUser) Button bAddUser;
    @BindView(R.id.lvUsers) ListView lvUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Cursor cursor = UsersDBHelper.getInstance(this).retrieveUsers();
        UserAdapter adapter = new UserAdapter(this.getApplicationContext(), cursor);
        this.lvUsers.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        UserAdapter adapter = (UserAdapter) this.lvUsers.getAdapter();
        adapter.getCursor().close();
    }

    @OnClick(R.id.bAddUser)
    public void addUser(){
        Random rnd = RandomGenerator.getInstance();
        String name = rnd.nextInt(1000000) + "_User";
        int age = rnd.nextInt(99);
        int points = rnd.nextInt(1000);
        User user = new User(name, age, points);

        UserAdapter adapter = (UserAdapter) lvUsers.getAdapter();
        adapter.insertUser(user);
    }


}
