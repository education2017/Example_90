package hr.ferit.bruno.example_90;

import java.util.Random;

/**
 * Created by Zoric on 19.9.2017..
 */

public class RandomGenerator {
    private static Random mRandomGenerator;

    private RandomGenerator(){
        mRandomGenerator = new Random();
    }

    public static synchronized Random getInstance(){
        if(mRandomGenerator == null){
            mRandomGenerator = new Random();
        }
        return mRandomGenerator;
    }
}
