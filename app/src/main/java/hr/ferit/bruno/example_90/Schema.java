package hr.ferit.bruno.example_90;

/**
 * Created by Zoric on 19.9.2017..
 */

public class Schema {
    // Database info:
    public static String DATABASE_NAME = "database";
    public static int DATABASE_VERSION = 1;

    // Database tables:
    public static String TABLE_USERS = "users";

    // Table column names:
    // USERS:
    public static String USERS_ID = "_id";
    public static String USERS_NAME = "name";
    public static String USERS_AGE = "age";
    public static String USERS_POINTS = "points";
}
