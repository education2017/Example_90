package hr.ferit.bruno.example_90;

/**
 * Created by Zoric on 19.9.2017..
 */

public class User {
    String mName;
    int mAge;
    int mPoints;

    public User() {
    }

    public User(String name, int age, int points) {
        mName = name;
        mAge = age;
        mPoints = points;
    }

    public String getName() {
        return mName;
    }

    public int getAge() {
        return mAge;
    }

    public int getPoints() {
        return mPoints;
    }

}
