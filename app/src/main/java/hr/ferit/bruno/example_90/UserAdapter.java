package hr.ferit.bruno.example_90;

import android.content.Context;
import android.database.Cursor;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zoric on 19.9.2017..
 */

public class UserAdapter extends CursorAdapter {

    UsersDBHelper mUsersDBHelper;

    public UserAdapter(Context context, Cursor cursor) {
        super(context.getApplicationContext(), cursor, false);
        this.mUsersDBHelper = UsersDBHelper.getInstance(context.getApplicationContext());
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.list_item_user,parent,false);
        UserViewHolder holder = new UserViewHolder(view);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        UserViewHolder holder = (UserViewHolder) view.getTag();
        String name = cursor.getString(cursor.getColumnIndex(Schema.USERS_NAME));
        int age = cursor.getInt(cursor.getColumnIndex(Schema.USERS_AGE));
        int points = cursor.getInt(cursor.getColumnIndex(Schema.USERS_POINTS));
        holder.tvUserName.setText(name);
        holder.tvUserPoints.setText(String.valueOf(points));
        holder.tvUserAge.setText(String.valueOf(age));
    }

    public void insertUser(User user) {
        this.mUsersDBHelper.insertUser(user);
        this.getCursor().close();
        // Do this on a separate thread:
        Cursor cursor = this.mUsersDBHelper.retrieveUsers();
        this.swapCursor(cursor);
    }

    static class UserViewHolder {
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvUserAge)
        TextView tvUserAge;
        @BindView(R.id.tvUserPoints)
        TextView tvUserPoints;

        public UserViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
